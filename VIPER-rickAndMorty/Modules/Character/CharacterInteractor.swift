//
//  CharacterInteractor.swift
//  VIPER-rickAndMorty
//
//  Created by jojo on 10/4/23.
//

import Foundation

final class CharacterInteractor {
    weak var presenter: CharacterInteractorToPresenterProtocol?
     var service: CharacterServiceProtocol?
}

extension CharacterInteractor: CharacterInteractorProtocol {
    func getCharacters() {
        service?.getCharacters { [weak self] charDto in
            print("👉\(charDto)👈")
            self?.presenter?.receiveChar(character: charDto.mapToUI())
        }
    }
}
