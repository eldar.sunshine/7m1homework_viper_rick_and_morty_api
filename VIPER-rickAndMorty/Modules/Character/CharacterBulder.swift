//
//  CharacterBulder.swift
//  VIPER-rickAndMorty
//
//  Created by jojo on 10/4/23.
//

import UIKit

final class CharacterBulder {
    class func build() -> UIViewController {
        let view = CharacterViewController()
        let presenter = CharacterPresenter()
        let interactor = CharacterInteractor()
        let router = CharacterRouter()
        let service = CharacterService.shared
        
        view.presenter = presenter
        
        presenter.view = view
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.presenter = presenter
        interactor.service = service
        
        router.viewController = view
        
        return view
    }
}
