//
//  CharacterViewController.swift
//  VIPER-rickAndMorty
//
//  Created by jojo on 10/4/23.
//

import UIKit

class CharacterViewController: UIViewController {
    
    var presenter: CharacterViewToPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.getCharacters()
    }
}

extension CharacterViewController: CharacterViewProtocol {
    
    func receiveCharacter(character: Character) {
        
    }
    
    func receiveError(error: Error) {
        
    }
}
