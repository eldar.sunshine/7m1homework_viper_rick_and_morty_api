//
//  Character.swift
//  VIPER-rickAndMorty
//
//  Created by jojo on 10/4/23.
//

import Foundation

struct Character {
    let results: [CharacterItems]
}

struct CharacterItems {
    let name: String?
}
