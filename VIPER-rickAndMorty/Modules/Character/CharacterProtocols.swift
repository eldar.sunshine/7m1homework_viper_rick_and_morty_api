//
//  CharacterProtocols.swift
//  VIPER-rickAndMorty
//
//  Created by jojo on 10/4/23.
//

import Foundation

protocol CharacterViewProtocol: AnyObject {
    func receiveCharacter(character: Character)
    func receiveError(error: Error)
}

protocol CharacterViewToPresenterProtocol: AnyObject {
    func getCharacters()
}

protocol CharacterInteractorProtocol: AnyObject {
    func getCharacters()
}

protocol CharacterInteractorToPresenterProtocol: AnyObject {
    func receiveChar(character: Character)
    func receiveError(error: Error)
}

protocol CharacterRouterProtocol: AnyObject {
    
}
