//
//  CharacterPresenter.swift
//  VIPER-rickAndMorty
//
//  Created by jojo on 10/4/23.
//

import Foundation

class CharacterPresenter {
    
    weak var view: CharacterViewProtocol?
    var interactor: CharacterInteractorProtocol?
    var router: CharacterRouterProtocol?
    
}

extension CharacterPresenter: CharacterViewToPresenterProtocol {
    
    func getCharacters() {
        interactor?.getCharacters()
    }

}

extension CharacterPresenter: CharacterInteractorToPresenterProtocol {
    func receiveChar(character: Character) {
        view?.receiveCharacter(character: character)
    }
    
    func receiveError(error: Error) {
        view?.receiveError(error: error)
    }
    
}
