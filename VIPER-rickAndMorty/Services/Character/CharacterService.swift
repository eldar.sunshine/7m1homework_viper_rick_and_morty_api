//
//  CharacterService.swift
//  VIPER-rickAndMorty
//
//  Created by jojo on 10/4/23.
//

import Foundation

protocol CharacterServiceProtocol {
    func getCharacters(completion: @escaping (CharacterDTO) -> ())
}

final class CharacterService: CharacterServiceProtocol {
    static let shared = CharacterService()
    let session = URLSession.shared
    let baseUrl = "https://rickandmortyapi.com/api/"
    
    func getCharacters(completion: @escaping (CharacterDTO) -> ()) {
        let request = URLRequest(url: URL(string: baseUrl + "character")!)
        
        let task = session.dataTask(with: request) { data, _, _ in
            DispatchQueue.main.async {
                guard let data else {return}
                guard let response = try? JSONDecoder().decode(CharacterDTO.self, from: data) else {
                    return
                }
                completion(response)
            }
        }
        task.resume()
    }
    
}
