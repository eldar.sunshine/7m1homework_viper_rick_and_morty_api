//
//  CharacterDTO.swift
//  VIPER-rickAndMorty
//
//  Created by jojo on 10/4/23.
//

import Foundation

struct CharacterDTO: Codable {
    let results: [CharacterItemsDTO]
}

struct CharacterItemsDTO: Codable {
    let name: String
}

extension CharacterDTO {
     func mapToUI() -> Character {
         let array =  self.results.map({ item in
             CharacterItems(name: item.name)
         })
         let char = Character(results: [])

         return char
    }
}





//// MARK: - CharacterDTO
//struct CharacterDTO: Codable {
//    let info: Info?
//    let results: [Result]?
//}
//
//// MARK: - Info
//struct Info: Codable {
//    let count, pages: Int?
//    let next, prev: String?
//}
//
//// MARK: - Result
//struct Result: Codable {
//    let id: Int?
//    let name: String?
//    let status: Status?
//    let species, type: String?
//    let gender: Gender?
//    let origin, location: Location?
//    let image: String?
//    let episode: [String]?
//    let url: String?
//    let created: String?
//}
//
//enum Gender: String, Codable {
//    case female = "Female"
//    case male = "Male"
//    case unknown = "unknown"
//}
//
//// MARK: - Location
//struct Location: Codable {
//    let name: String?
//    let url: String?
//}
//
//enum Status: String, Codable {
//    case alive = "Alive"
//    case dead = "Dead"
//    case unknown = "unknown"
//}
