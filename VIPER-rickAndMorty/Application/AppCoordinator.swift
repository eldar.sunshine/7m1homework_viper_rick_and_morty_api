//
//  AppCoordinator.swift
//  VIPER-rickAndMorty
//
//  Created by jojo on 10/4/23.
//

import Foundation
import UIKit

final class AppCoordinator {
    
    private let window: UIWindow?
    
    init(window: UIWindow?) {
        self.window = window
    }
    
    func start() {
        openRootViewController()
    }
    
    private func openRootViewController() {
        window?.rootViewController = UINavigationController(rootViewController: CharacterBulder.build())
    }
}
